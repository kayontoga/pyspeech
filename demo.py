#!/usr/bin/env python3
#
# -*- coding: utf-8 -*-
#
# Demonstrate speech to text from a variety of services. For most a
# key is required, which is prompted for and if not available, skip
# that service.
#
# Based on https://github.com/Uberi/speech_recognition/blob/master/examples/audio_transcribe.py

import sys
import argparse

import speech_recognition as sr

from os import path
from mlhub.utils import get_cmd_cwd

DEFAULT_PATH = path.join(path.dirname(path.realpath(__file__)),
                         "whatstheweatherlike.wav")

option_parser = argparse.ArgumentParser(prog='demo')

option_parser.add_argument(
    'path',
    nargs="?",
    help='path to audio file to transcribe')

args = option_parser.parse_args()

if args.path is None:
    args.path = DEFAULT_PATH
else:
    args.path = path.join(get_cmd_cwd(), args.path)

r = sr.Recognizer()

# Read the entire audio file.

with sr.AudioFile(args.path) as source:
    audio = r.record(source)

# Recognise with tensorflow
#
# Is this an available model? It's in the github source code but not
# the pypi package.

try:
    print(r.recognize_tensorflow(audio))
except AttributeError:
    print("Tensorflow model not available. Skipping.\n")

# Recognise speech using Sphinx if it is installed.

try:
    print("Sphinx identifies\n=====> " + r.recognize_sphinx(audio))
except sr.UnknownValueError:
    print("Sphinx could not understand audio")
except sr.RequestError as e:
    print("Sphinx does not seem to be installed. Skipping.\n")

# Recognise speech using Google Speech Recognition.
#
# For demonstration use the default API key.  Another API key can
# be used with
#
# r.recognize_google(audio, key="GOOGLE_SPEECH_RECOGNITION_API_KEY")
#
# instead of
#
# r.recognize_google(audio)

try:
    print("Google Speech Recognition identifies:\n=====> " +
          r.recognize_google(audio) + "\n")
except sr.UnknownValueError:
    print("Google Speech Recognition could not understand audio")
except sr.RequestError as e:
    print("Could not request results from Google Speech Recognition service; {0}".
          format(e))

# Recognise speech using Google Cloud Speech.
#
# Requires the contents of a JSON file to be read in. Not yet
# implemented.

sys.stdout.write("Google Cloud Speech credentials (json filename): ")
fname = input()

if len(fname) > 0:
    GOOGLE_CLOUD_SPEECH_CREDENTIALS = read.input(fname)
    try:
        print("Google Cloud Speech identifies:\n=====> " +
              r.recognize_google_cloud(audio, credentials_json=GOOGLE_CLOUD_SPEECH_CREDENTIALS))
    except sr.UnknownValueError:
        print("Google Cloud Speech could not understand audio")
    except sr.RequestError as e:
        print("Could not request results from Google Cloud Speech service; {0}".format(e))
else:
    print("Skipping\n")

# Recognise speech using Wit.ai
#
# Wit.ai keys are 32-character uppercase alphanumeric strings.

sys.stdout.write("WIT.ai key (32 character uppercase): ")
key = input()

if len(key) > 0:
    WIT_AI_KEY = key
    try:
        print("Wit.ai identifies:\n=====> " + r.recognize_wit(audio, key=WIT_AI_KEY))
    except sr.UnknownValueError:
        print("Wit.ai could not understand audio")
    except sr.RequestError as e:
        print("Could not request results from Wit.ai service; {0}".format(e))
else:
    print("Skipping\n")

# Recognise speech using Azure API - Not Currently Available from
# PyPi but it is there on Github?

sys.stdout.write("Azure Speech API key (32 character): ")
key = input()

if len(key) > 0:
    AZURE_SPEECH_KEY = key
    try:
        print("Microsoft Azure Speech identifies:\n=====> " +
              r.recognize_azure(audio, key=AZURE_SPEECH_KEY))
    except sr.UnknownValueError:
        print("Microsoft Azure Speech could not understand audio")
    except sr.RequestError as e:
        print("Could not request results from Microsoft Azure Speech service; {0}".format(e))
else:
    print("Skipping\n")


# Recognize speech using Microsoft Bing Voice Recognition

sys.stdout.write("Bing Voice Recognition API key (32 character): ")
key = input()

if len(key) > 0:
    BING_KEY = key  # Microsoft Bing Voice Recognition API keys 32-character lowercase hexadecimal strings
    try:
        print("Microsoft Bing Voice Recognition identifies:\n=====> " +
              r.recognize_bing(audio, key=BING_KEY))
    except sr.UnknownValueError:
        print("Microsoft Bing Voice Recognition could not understand audio")
    except sr.RequestError as e:
        print("Could not request results from Microsoft Bing Voice Recognition service; {0}".format(e))
else:
    print("Skipping\n")

# Recognise speech using Houndify.

sys.stdout.write("Houndify client ID (base64-encoded string): ")
ident = input()

if len(ident) > 0:
    sys.stdout.write("Houndify client key (base64-encoded string): ")
    key = input()

    if len(key) > 0:
        HOUNDIFY_CLIENT_ID = ident
        HOUNDIFY_CLIENT_KEY = key
        try:
            print("Houndify identifies:\n=====> " +
                  r.recognize_houndify(audio, client_id=HOUNDIFY_CLIENT_ID, client_key=HOUNDIFY_CLIENT_KEY))
        except sr.UnknownValueError:
            print("Houndify could not understand audio")
        except sr.RequestError as e:
            print("Could not request results from Houndify service; {0}".format(e))
    else:
        print("Skipping\n")
else:            
    print("Skipping\n")

# Recognise speech using IBM Speech to Text
#
# IBM Speech to Text usernames are strings of the form XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX
# IBM Speech to Text passwords are mixed-case alphanumeric strings

sys.stdout.write("IBM Speech to Text username: ")
user = input()

if len(user) > 0:
    sys.stdout.write("IBM Speech to Text password: ")
    key = input()

    if len(key) > 0:
        IBM_USERNAME = user
        IBM_PASSWORD = key
        try:
            print("IBM Speech to Text identifies:\n=====> " +
                  r.recognize_ibm(audio, username=IBM_USERNAME, password=IBM_PASSWORD))
        except sr.UnknownValueError:
            print("IBM Speech to Text could not understand audio")
        except sr.RequestError as e:
            print("Could not request results from IBM Speech to Text service; {0}".format(e))
    else:
        print("Skipping\n")
else:            
    print("Skipping\n")
