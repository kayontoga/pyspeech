Speech to Text
==============

This [MLHub](https://mlhub.ai) package introduces and demonstrates the
concept of speech to text. It supports a command line tool to
*transcribe* an audio file using a free google service. The *demo*
supports various commercial and closed source services.

Visit the gitlab repository for more details:
<https://gitlab.com/kayontoga/pyspeech>

Quick Start Command Line Examples
---------------------------------

After installing and configuring the package:

```console
$ play ~/.mlhub/pyspeech/whatstheweatherlike.wav
$ ml transcribe pyspeech ~/.mlhub/pyspeech/whatstheweatherlike.wav 
```

Usage
-----

- To install mlhub (Ubuntu):

```console
$ pip3 install mlhub
$ ml configure
```

- To install and configure the demo:

```console
$ ml install pyspeech
$ ml configure pyspeech
$ ml readme pyspeech
$ ml commands pyspeech
```

Resources
---------

* [github](https://github.com/Uberi/speech_recognition)

* [pypi](https://pypi.org/project/SpeechRecognition/)
