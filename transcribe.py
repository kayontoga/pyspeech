#!/usr/bin/env python3
#
# -*- coding: utf-8 -*-
#
# ml transcribe pyspeech <path>

import argparse

import speech_recognition as sr

from os import path
from mlhub.utils import get_cmd_cwd

option_parser = argparse.ArgumentParser(add_help=False)

option_parser.add_argument(
    'path',
    help='path to audio file to transcribe')

args = option_parser.parse_args()

args.path = path.join(get_cmd_cwd(), args.path)

# Initialize recognizer class (for recognizing the speech).

r = sr.Recognizer()

# Read the audio file as source, listening the audio file and store in
# audio_text variable.

with sr.AudioFile(args.path) as source:
    
    audio_text = r.listen(source)
    
    # Recognise using google method.
    text = r.recognize_google(audio_text)
    print(text)
